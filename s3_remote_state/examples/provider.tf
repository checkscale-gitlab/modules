provider "aws" {
  region                  = var.aws_region
  shared_credentials_file = "auth/credentials"
  profile                 = var.aws_profile
}

