# On terraform block variables cannot be used
terraform {
  backend "s3" {
    bucket         = "acme-terraform-state-prod"
    key            = "s3/global/terraform.tfstate"
    region         = "us-east-2"    # Replace this with your DynamoDB table name!
    dynamodb_table = "terraform-locks-prod"
    encrypt        = true
    profile        = "acme"
  }
}

module "s3_remote_state" {
#  source                = "git::git@gitlab.com:live9/terraform/modules.git///s3_remote_state"
  source                = "../../modules/s3_remote_state"
  environment           = var.environment
  aws_region            = var.aws_region
  force_destroy         = var.force_destroy
}
