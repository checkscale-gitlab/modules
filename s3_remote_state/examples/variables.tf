variable "aws_profile" {
  description = "AWS profile"
  default     = "acme"
}

variable "aws_region" {
  description = "AWS region on which we will setup the swarm cluster"
  default     = "us-east-2"
}

variable "environment" {
  description = "Deplyment environment"
  default     = "staging"
}

variable "project" {
  description = "Project name"
  default     = "Example"
}

variable "force_destroy" {
  description = "Delete objects from bucket when its not empty"
}
