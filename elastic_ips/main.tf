resource "aws_eip" "eip" {
  vpc   = true
  count = var.eip_count

  tags = {
    Name = "eip-${var.environment}-${count.index + 1}"
  }
}
