variable "environment" {
  description = "Environment name"
  #default = "staging"
}

variable "project" {
  description = "Project name"
}

variable "aws_region" {
  description = "AWS region on which we will setup the swarm cluster"
  default     = "us-east-2"
}

variable "public_subnets_cidr" {
  description = "List of the cluster's public subnets"
  type        = list(any)
  default     = ["10.0.10.0/24", "10.0.20.0/24", "10.0.30.0/24"]
}

variable "private_subnets_cidr" {
  description = "List of the cluster's private subnets"
  type        = list(any)
  default     = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
}

variable "manager_count" {
  description = "Managers amount"
  default     = "1"
}

variable "worker_count" {
  description = "Workers amount"
  default     = "1"
}

variable "manager_ami" {
  description = "Debian AMI"
  # para us-east-2, ubuntu 20.04 Amazon
  #default= "ami-07fb7bd53bacdfc16"
  #debian 10.9 buster
  default = "ami-089fe97bc00bff7cc"
}

variable "worker_ami" {
  description = "Ubuntu 20.04 LTS AMI"
  # para us-east-2, ubuntu 20.04 Amazon
  #default= "ami-07fb7bd53bacdfc16"
  #debian 10.9 buster
  default = "ami-089fe97bc00bff7cc"
}

variable "manager_instance_type" {
  description = "Swarm Manager Instance type"
  default     = "t3.small"
}

variable "worker_instance_type" {
  description = "Swarm worker Instance type"
  default     = "t3.small"
}

variable "workers_public_ip" {
  description = "Define use of public ips for worker nodes"
  default     = false
}

variable "ssh_key_name" {
  description = "ssh key name"
}

variable "key_path" {
  description = "SSH Public Key path"
}

variable "private_key_default" {
  description = "SSH Public Key path"
}


variable "bootstrap_path" {
  description = "Script to provision instance"
  default     = "scripts/provision_instance.sh"
}

variable "manager_disk_size" {
  description = "Manager nodes hdd size in GB"
  default     = "50"
}

variable "manager_disk_type" {
  description = "Manager nodes hdd type"
  default     = "standard"
}

variable "worker_disk_size" {
  description = "Worker nodes hdd size in GB"
  default     = "50"
}

variable "worker_disk_type" {
  description = "Worker nodes hdd type"
  default     = "standard"
}

variable "ssh_user" {
  description = "User for ssh provisioning"
  #default = "ubuntu"
  default = "admin"
}

variable "create_efs_sg" {
  description = "Create EFS security to assign to EFS mounts"
  default     = false
}

variable "create_nfs_sg" {
  description = "Create NFS security to assign to Manager node"
  default     = false
}

variable "external_eips" {
  description = "Defeine if elastic ips should be created externally or not"
  default     = false
}

variable "extra_security_groups" {
  description = "Add additional security groups to cluster"
  type        = list(any)
  default     = []
}
