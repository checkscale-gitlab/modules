module "swarm_cluster" {
  source                = "../"
  environment           = var.environment
  project               = var.project
  aws_region            = var.aws_region
  manager_count         = var.manager_count
  worker_count          = var.worker_count
  manager_instance_type = var.manager_instance_type
  worker_instance_type  = var.worker_instance_type
  key_path              = var.key_path
  private_key_default   = var.key_path_private
  ssh_key_name          = local.ssh_key_name
  create_nfs_sg         = var.create_nfs_sg
  external_eips         = var.external_eips
}
